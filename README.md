Command line instructions


Git global setup

git config --global user.name "JohnJonathas"
git config --global user.email "jls@ic.ufal.br"

Create a new repository

git clone git@gitlab.com:JohnJonathas/PSE_Projects.git
cd PSE_Projects
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin git@gitlab.com:JohnJonathas/PSE_Projects.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin git@gitlab.com:JohnJonathas/PSE_Projects.git
git push -u origin --all
git push -u origin --tags