#ifndef _CIRCULAR_BUFFER_H
#define _CIRCULAR_BUFFER_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct {
  uint8_t *buffer;
  int head;
  int tail;
  int size;
} buffer_t;

void init(buffer_t *buffer, uint8_t *data, int size);

int is_full(buffer_t *buffer);

int is_empty(buffer_t *buffer);

int push(buffer_t *buffer, uint8_t data);

#endif
