#include "circular_buffer.h"

void init(buffer_t *buffer, uint8_t *data, int size){
    buffer->buffer = data;
    buffer->head = 0;
    buffer->tail = 0;
    buffer->size = size;
}

int is_full(buffer_t *buffer) {
  int next = buffer->head + 1;
  if (next >= buffer->size) next = 0;
  if (next == buffer->tail) return 1;
  else return 0;
}

int is_empty(buffer_t *buffer) {
  if (buffer->head == buffer->tail) return 1;
  else return 0;
}

int push(buffer_t *buffer, uint8_t data){
  int next = buffer->head + 1;
  if (next >= buffer->size) next = 0;

  if (next == buffer->tail)                         // check if circular buffer is full
  return -1;                                        // and return with an error.

  buffer->buffer[buffer->head] = data;              // Load data and then move
  buffer->head = next;
  return 0;
}
