#include "circular_buffer.h"

#define SIZE_ 80

void sleep(time_t delay){
	time_t timer0, timer1;
	time(&timer0);
	do{
		time(&timer1);
	}
	while((timer1-timer0) < delay);
}

int main (void){
	buffer_t buffer;
	uint8_t data[SIZE_]; 
  	init(&buffer, &data, SIZE_);
	int i = 0;
	int ok = 0;
    time_t t;
	int data_temp[SIZE_/4]; 
	do{
		uint8_t dados = (uint8_t)(rand() % 255);
		ok = push(&buffer,dados);
		printf("number %d = %d\n",i,buffer.buffer[i++]);
		sleep(1);
	} 
	while(ok!=-1);

	memcpy(&data_temp, &data, SIZE_);

	for(i = 0 ; i < SIZE_ / 4 ; i++)
	{
		printf("number in hex %d = %x\n", i+1,(char)data_temp[i]);
	}

	printf("Saindo\n");

	return 0;

}